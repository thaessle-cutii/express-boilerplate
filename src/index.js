import express from 'express';
import winston from 'winston';
import bodyParser from 'body-parser';

import db from './models';
import apiRoutes from './routes';

//initialize express http server
const app = express();

//setup logger
winston.level = process.env.LEVEL || 'info';
winston.add(winston.transports.File, { filename: __dirname + `/logs/server.${new Date().toISOString()}.log` });

winston.debug('winston is set up');

//setup bodyParser
app.use(bodyParser.urlencoded({ extended: true, limit : '2mb' }));
app.use(bodyParser.json({ limit : '2mb' }));

// connect the api routes under /api/*
app.use('/api', apiRoutes);

//start http server
const port = process.env.PORT || 5000 ;
db.sequelize.sync().then(() => {
  const server = app.listen(port, (err) => {
      if (err) {
        winston.error(err);
        return;
      }
      winston.info(` 🌎  API is listening on port ${ server.address().port } `);
  });
});
