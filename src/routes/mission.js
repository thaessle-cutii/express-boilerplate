import express from 'express';
import winston from 'winston';
import _ from 'lodash';

import { Mission } from '../models';
const apiRoutes = express.Router();

/**
* @api {get} /mission/:id Get mission
* @apiVersion 1.0.0
* @apiName GetMission
* @apiGroup Mission
* @apiDescription This API Method is used to get a mission
*
* @apiParam {STRING} id Activity id
*
* @apiSuccess {JSON} mission Detail informations about the Mission.
*/
apiRoutes
  .get('/:id', (req, res) => {
    const missionId = req.params.id;
    if (!missionId) {
      return res.status(400).send({success : false, msg : 'Mission id needed'});
    }
    Mission.findMissionById(missionId)
      .then(mission => res.status(200).send({success : true, msg : 'Mission fetched', mission}))
      .catch(err => {
        winston.error(`Error on GET /mission/${missionId} : ${err}`);
        winston.debug(`Error on GET /mission/${missionId}  : ${err.stack}`);
        res.status(500).send({success : false, msg : 'Mission not fetched'});
      });
  });

/**
* @api {post} /mission Mission creation
* @apiVersion 1.0.0
* @apiName PostMission
* @apiGroup Mission
* @apiDescription This API Method is used to create a mission
*
* @apiParam {STRING} name Name of the Activity
* @apiParam {STRING} description description of the mission,
* @apiParam {STRING} objective objective of the mission,
* @apiParam {DECIMAL} reward reward for the mission,
* @apiParam {STRING='development', 'design', 'marketing', 'testing'} type Category of the mission.
* @apiParam {STRING} imageUrl url.
*
* @apiSuccess {JSON} mission Detail informations about the Activity.
*/
apiRoutes
  .post('/', (req, res) => {
    //only pick attributes of Mission model
    const newMission  = _.omit(_.pick(req.body, _.keys(Mission.attributes)), ['updatedAt', 'createdAt']);
    if (!newMission.name || !newMission.description || !newMission.reward) {
       winston.info('Attempt to create mission witout required fields !');
       winston.debug(`Request body ${req.body}`);
       return res.status(403).send({success: false, msg: 'Required field are missing !'});
    }
    //set default status if omited
    newMission.status = newMission.status || 'draft';
    const updateOrCreate = () => {
      if (newMission.id) {
        return Mission.update(newMission, {
          where: {id: newMission.id},
          returning: true,
        });
      }
      else {
        return Mission.create(newMission);
      }
    };

    return updateOrCreate()
      .then((result) => {
        //create return a Mission object but update return an array
        //result.name is a tweak to test if result is a Mission object
        let mission = {};
        (result.name) ? mission = result : mission = result[1][0] ;
        winston.info(`Create or update a mission with id ${mission.id}`);
        return res.status(200).send({ success : true, msg : 'Mission created', mission : mission });}
      )
      .catch(err => {
        winston.error(`Error on POST /mission : ${err}`);
        winston.debug(`Error on POST /mission : ${err.stack}`);
        return res.status(400).send({success : false, msg : 'Mission creation failed'});
      });
  });


export default apiRoutes;
