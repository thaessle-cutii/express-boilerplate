import express from 'express';
import missionRoutes from './mission';

const apiRoutes = express.Router();

apiRoutes.use('/mission', missionRoutes);

module.exports = apiRoutes;
