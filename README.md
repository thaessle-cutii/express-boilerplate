# Express boilerplate

Training project to build API backend services

## Informations

### What you need to start :
* [nodeJS v6.9.x & NPM](https://nodejs.org/en/download/) :
* [Git](https://git-scm.com/)
* [Gitlab account](https://gitlab.com/)

Clone the project : `git clone git@gitlab.com:thaessle/express-boilerplate.git`

Install the dependencies : `npm i`

Let's hacking : `npm run dev`

### Libraries used in this project :
* [Babel](https://babeljs.io/) : JavaScript Polyfill for ES2015, ES2016, ES2017
* [Express](http://expressjs.com/) : Fast, unopinionated, minimalist web framework for node.js
* [Sequelize](http://docs.sequelizejs.com/) : ORM for Node.js
* [winston](https://github.com/winstonjs/winston) : Async logging library for node.js
* [lodash](https://lodash.com/docs/) : JavaScript utility library

### NPM Scripts :
* Run unit tests : `npm run test`
* Run linters : `npm run lint`
* Build documentation : `npm run doc`
* Run platform for dev with hot reload : `npm run dev`
* Run platform for production : `npm start`

### Environment variables :
For development environment you can set variables in the .env file
* LEVEL : level for loging (debug, info, warn, error)
* PORT : port to start node server (default: 5000)
* PG_USERNAME : username to connect PG Database
* PG_PASSWORD : password to connect PG Database
* PG_DATABASE : database name
* PG_HOST : database server host
* PG_PORT : database server port

## Exercice
1. Add PUT (alter or create) routes on MISSION with tests
2. Add a USER model : a USER is a person who can publish many MISSION
3. Add GET, POST (create) & PUT routes on USER with tests
4. A USER can also apply to many MISSION : add an association with sequelize
5. Add routes to apply to missions

## Developer materials

Some stuff to be a better hacker

![80's hacker](http://i.giphy.com/l46C6sdSa5DVSJnLG.gif)

### Usefull tools :
* [Atom](https://atom.io/) : Text Editor
* PostgreSQL :
  * [Postgres.app](http://postgresapp.com/) : PosgreSQL Sever on Mac
  * [Postgres Server](http://www.enterprisedb.com/products-services-training/pgdownload) : PosgreSQL Sever on Linux or Windows
  * [PgAdmin](https://www.pgadmin.org/) : PosgreSQL tools
* [Postman](https://www.getpostman.com/)
* [Gitmoji](https://gitmoji.carloscuesta.me/) : emojis for your commit messages
